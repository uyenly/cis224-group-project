//jQuery Button Demo
//Name: Uyen Ly
//Date: 2/16/17

/*
window.onload=function(){
    var wise = document.getElementById("wiseMonkey");
    wise.innerHTML="Wise monkey says hello";
    
}
*/

$(document).ready(function(){
    
    var wiseDiv=$("#wiseMonkeyFirst");
    var wiseDiv2=$("#wiseMonkeySecond");
    var monkeyMessage=$("#message");
    var monkeyPic;
    var emptyDiv="url(../media/img/empty.png)";
    
    $("button").each(function(){
        $(this).click(function(){
            monkeyPic = $(this).find("img").attr("src");
            var url = "url(" + monkeyPic + ")";
            if (wiseDiv.css('background-image').includes('empty'))
            {
                wiseDiv.css("background-image",url);   
                
            } else if (wiseDiv2.css('background-image').includes('empty'))
            {
                wiseDiv2.css('background-image',url);
                if (wiseDiv.css("background-image") === wiseDiv2.css("background-image"))
                {
                    monkeyMessage.html($(this).find("img").attr("alt"));                    
                } else if (wiseDiv.css("background-image") !== wiseDiv2.css("background-image"))
                {
                    monkeyMessage.html("That was unwise");
                }
            }else
            {
                wiseDiv.css("background-image", url);
                wiseDiv2.css("background-image",emptyDiv);
                monkeyMessage.html("");
            }
            
        });
    });
        
   
});